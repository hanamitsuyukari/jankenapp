package com.example.jankenapp;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class DisplayView extends View {
    private Bitmap myBitmap;
    private Bitmap cpuBitmap;

    public DisplayView(Context context) {
        super(context);
    }

    public DisplayView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DisplayView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        if(myBitmap == null || cpuBitmap == null){
            return;
        }
        canvas.drawBitmap(myBitmap,getWidth()/3,getHeight()/2,null);
        canvas.drawBitmap(cpuBitmap,getWidth()/3,getHeight()/4,null);
    }

    public void setChoice(int myNo,int cpuNo){
        int mySelectId =
        myNo == 0 ? R.drawable.my_gu : myNo == 1 ? R.drawable.my_choki : R.drawable.my_par;

        int cpuSelectId=
        cpuNo == 0 ? R.drawable.cpu_gu : cpuNo == 1 ? R.drawable.cpu_choki: R.drawable.cpu_par;

        Resources res = getResources();
        myBitmap = BitmapFactory.decodeResource(res,mySelectId);
        cpuBitmap = BitmapFactory.decodeResource(res,cpuSelectId);
        invalidate();

    }
}
