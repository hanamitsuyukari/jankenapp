package com.example.jankenapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    DisplayView displayView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        displayView = findViewById(R.id.displayView);
        ImageButton gu = findViewById(R.id.imageButton4);
        ImageButton choki = findViewById(R.id.imageButton2);
        ImageButton pa = findViewById(R.id.imageButton3);
        gu.setOnClickListener(this);
        choki.setOnClickListener(this);
        pa.setOnClickListener(this);

        displayResult();
    }

    @Override
    public void onClick(View v) {
        int myNo =0;
        int cpuNo = CPU.getChoice();

        switch (v.getId()){
            case R.id.imageButton4:
                myNo = 0;
                break;
            case R.id.imageButton2:
                myNo = 1;
                break;
            case R.id.imageButton3:
                myNo = 2;
                break;
        }
        displayView.setChoice(myNo,cpuNo);
        setResult(myNo,cpuNo);
        displayResult();
    }

    public void displayResult(){
        String fileName = "janken";
        SharedPreferences sp = getSharedPreferences(fileName,MODE_PRIVATE);

        int win = sp.getInt("WIN",0);
        int lost = sp.getInt("LOST",0);
        int draw = sp.getInt("DRAW",0);

        String result = win + "勝" + lost +  "敗" + draw + "分";
        TextView text = findViewById(R.id.textView);
        text.setText(result);
    }

    public void setResult(int myNo,int cpuNo){
        String fileName = "janken";
        SharedPreferences sp = getSharedPreferences(fileName,MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        int win = sp.getInt("WIN",0);
        int lost = sp.getInt("LOST",0);
        int draw = sp.getInt("DRAW",0);

        switch (myNo){
            case 0:
                if(cpuNo == 0){
                    draw++;
                }else if(cpuNo == 1){
                    win++;
                }else{
                    lost++;
                }
                break;
            case 1:
                if(cpuNo == 0){
                    lost++;
                }else if(cpuNo == 1){
                    draw++;
                }else{
                    win++;
                }
                break;
            case 2:
                if(cpuNo == 0){
                    win++;
                }else if(cpuNo == 1){
                    lost++;
                }else{
                    draw++;
                }
                break;
        }
        editor.putInt("WIN",win);
        editor.putInt("LOST",lost);
        editor.putInt("DRAW",draw);
        editor.commit();
    }
}
